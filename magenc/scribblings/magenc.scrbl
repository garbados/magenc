#lang scribble/manual

@title{Magenc Magnet URIs: Secure Object Permanence for the Web}
@author{Christopher Lemmer Webber}

@section{Introduction and Tutorial}

@hyperlink["https://gitlab.com/dustyweb/magenc/blob/master/magenc/scribblings/intro.org"]{@tt{magenc/scribblings/intro.org}}
